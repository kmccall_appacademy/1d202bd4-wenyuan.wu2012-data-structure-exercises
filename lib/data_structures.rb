# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  number_of_vowels = 0
  ['a','e','i','o','u','A','E','I','O','U'].each do |vowel|
    number_of_vowels += str.count (vowel)
  end
  number_of_vowels
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str.delete('aeiouAEIOU')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int_in_arr = []
  if int == 0
    return ["0"]
  else
    while int != 0
      int_in_arr << (int % 10).to_s
      int /= 10
    end
  end
  int_in_arr.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str_lowercase = str.downcase
  repeating_letters = false
  str_lowercase.split("").each do |char|
    if str_lowercase.count(char)>1
      repeating_letters = true
      break
    end
  end
  repeating_letters
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  first_three_digit_str = arr[0..2].join
  middle_three_digit_str = arr[3..5].join
  last_four_digit_str = arr[-4..-1].join
  "(#{first_three_digit_str}) #{middle_three_digit_str}-#{last_four_digit_str}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  str_arr = str.split(",")
  str_arr.max.to_i - str_arr.min.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  arr.drop(offset%4) + arr.take(offset%4)
end
